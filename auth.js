// db1.js
var MongoClient = require('mongodb').MongoClient;
var mongoUrl = "mongodb://mongodb/";
var secret = Buffer.from('fe1a1915a379f3be5394b64d14794932', 'hex');
var jwt = require('jsonwebtoken');
var express = require('express')
const authGuard = express.Router(); 

exports.secret = secret;

module.exports = {
    getSecret: () => {
        return secret;
    },
    FindUser: async function (data) {
        let client, db;
        try {
            client = await MongoClient.connect(mongoUrl, {
                useNewUrlParser: true
            });
            db = client.db("practitioner");
            let dCollection = db.collection('users');
            let result = await dCollection.findOne(data);
            return result;
        } catch (err) {
            console.error(err);
        } // catch any mongo error here
        finally {
            client.close();
        } // make sure to close your connection after
    },
    verifyToken: () => {
        return authGuard.use((req, res, next) => {
            var token = req.headers["authorization"];
            if(token == null || token == "") {
                return res.json({success: false, msg: "No estás logeado"});
            }
            token = token.replace('Bearer ', '');
            jwt.verify(token, secret, function (err, user) {
                if (err) {
                    return res.json({success: false, msg: "No estás logeado"});
                } else {
                    next();
                }
            });
        });
    },
    getUserId(token) {
        return jwt.decode(token, secret)["_id"];
    },
    SignUp: async function (data) {
        let client, db;
        try {
            client = await MongoClient.connect(mongoUrl, {
                useNewUrlParser: true
            });
            db = client.db("practitioner");
            let dCollection = db.collection('users');
            let duplicated = await dCollection.findOne({"email": data.email});
            if(duplicated) {
                return {"success": false, "msg": "¡Correo ya registrado!"};
            }
            let result = await dCollection.insertOne(data);

            let emptyAccount = {
                "nombre" : "Cuenta de cheques",
                "numero" : Math.floor(Math.random() * 10000000000000000).toString(),
                "userId" : result.insertedId,
                "saldo" : 0,
                "movimientos" : []
            }
            let accountCollection = db.collection('cuentas');
            await accountCollection.insertOne(emptyAccount);
            let user = result.ops[0];
            delete user["password"];
            var token = jwt.sign(user, this.getSecret(), {expiresIn: 60 * 60 * 24});
            return {"success": true, "msg": "¡Registrado correctamente!", "token": token};
        } catch (err) {
            console.error(err);
        } // catch any mongo error here
        finally {
            client.close();
        } // make sure to close your connection after
    }
}